//funksjon deffinesjon
#include "Objekter.h"

// her er objekter.cpp som jeg har laget 

Objekter::Objekter(void)
{
}

Objekter::Objekter(string name) //implimenterer 
{
	if (name == "Sukkerbit")
	{
		setType(name); //setter type her slik at vi får tilgang til alle navner "name"

		setHSVmin(Scalar(14, 0, 51)); //000 for det er det minimume HSV verdien som vi kan få. Også må vi finne ut hvilket HSVmin verdi sukkerbiten vår har
		setHSVmaks(Scalar(119, 228, 255)); //255 er den maksimale HSV verdien som vi kan få. Også må vi finne ut hvilket HSVmaks verdi sukkerbiten har
		
		//fjernet sukkerbit fra  Sukkerbit.setHSVmin(Scalar(54, 19, 45)); for vi er in Objekter.cpp filen 
		//her fant jeg ut at i OpenCV RGB er konvertert til BGR.
		setFarge(Scalar(255, 128, 0)); //setter HSV verdier for en farge som teksten skal jeg velger blå farge 
	}
	if (name == "Regulator")
	{
		setType(name); //setter type her slik at vi får tilgang til alle navner "name"

		setHSVmin(Scalar(54, 19, 45)); //000 for det er det minimume HSV verdien som vi kan få. Også må vi finne ut hvilket HSVmin verdi sukkerbiten vår har
		setHSVmaks(Scalar(107, 255, 196)); //255 er den maksimale HSV verdien som vi kan få. Også må vi finne ut hvilket HSVmaks verdi sukkerbiten har

										   
		setFarge(Scalar(255, 128, 0)); //setter HSV verdier for en farge som teksten skal jeg velger blå farge 
	}
	if (name == "Reguleringshjul")
	{
		setType(name); //setter type her slik at vi får tilgang til alle navner "name"

		setHSVmin(Scalar(85, 0, 3)); //000 for det er det minimume HSV verdien som vi kan få. Også må vi finne ut hvilket HSVmin verdi sukkerbiten vår har
		setHSVmaks(Scalar(167, 255, 99)); //255 er den maksimale HSV verdien som vi kan få. Også må vi finne ut hvilket HSVmaks verdi sukkerbiten har

										   //fjernet sukkerbit fra  Sukkerbit.setHSVmin(Scalar(54, 19, 45)); for vi er in Objekter.cpp filen 
										   //her fant jeg ut at i OpenCV RGB er konvertert til BGR.
		setFarge(Scalar(255, 128, 0)); //setter HSV verdier for en farge som teksten skal jeg velger blå farge 
	}
}

Objekter::~Objekter(void)
{
}

int Objekter::getXPos() //den er funksjonen som skal retunere integer 
{
	return Objekter::xPos;

}

void Objekter::setXPos(int x) // denne funksjonen skal ta variabler som blir sendt til og sette XPos til x :D
{
	Objekter::xPos = x;
	//xPos = x;
}


// gjør det samme med y 
int Objekter::getYPos() //den er funksjonen som skal retunere integer 
{
	return Objekter::yPos;
}

void Objekter::setYPos(int y) // denne funksjonen skal ta variabler som blir sendt til og sette XPos til x :D
{
	Objekter::yPos = y;
}


Scalar Objekter::getHSVmin() //først tar vi våre mottakk "geters" 
{
	return Objekter::HSVmin;
}


Scalar Objekter::getHSVmaks()
{
	return Objekter::HSVmaks;
}


void Objekter::setHSVmin(Scalar min) // setter vi den mottatte "geters"
{
	Objekter::HSVmin = min;

}

void Objekter::setHSVmaks(Scalar maks)
{
	Objekter::HSVmaks = maks;
}






