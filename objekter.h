//heder ==>  funksjonen erklæring "declaration"
// vi må inkudere dette i main programmet 
// detter er header filen jeg har lagt ti  


#pragma once
#include <string>
#include <opencv\cv.h>
#include<opencv\highgui.h>

using namespace std; //for å forenkle c++ komanduer linje 14 f.eks
using namespace cv; // bruker denne for å slippe å skrive cv::   gjennom hele prosjektet 


class Objekter
{
public: 

	Objekter(void);
	~Objekter(void);


	Objekter(string name ); // det er name argument 

	int getXPos();  // for xpos og ypos in i source.cpp vi trenger publik variabler slik at vi får tilgang til private variabler 
	void setXPos(int x);

	int getYPos();	
	void setYPos(int y);

	Scalar getHSVmin();
	Scalar getHSVmaks();

	void setHSVmin(Scalar min); //altid void funksjon for setters. hver gang vi har setters tar vi argumenten (the type of variable vi ar setting)
	void setHSVmaks(Scalar maks);
 
	
	string getType()	//geters and setters for type  er nødvendig 
	{
		return type;
	}
	void setType(string t)  
	{ 
		type = t; 
	}

	//geters og setters for teksten vi mottar 
	Scalar getFarge()
	{
		return Farge;
	}
	void setFarge(Scalar c)
	{
		Farge = c; 
	}


private: // pivatiserer informasjon slik at  vi får ikke tilgang til dem fra andre filene våret for vi ønsker ikke å forandre dem


	int xPos, yPos;
	string type;  //motar type som string
//	std::string type;

	Scalar HSVmin, HSVmaks;
		
	//for å få tekst på skjermen
	Scalar Farge;  //RGB
	  


};

